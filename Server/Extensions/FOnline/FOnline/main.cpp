#ifndef MAIN
#define MAIN

#include "main.h"

// apparently dll loading is under rework in the engine so this feature is still muted, revision 840
int __stdcall DllMain(void* module, unsigned long reason, void* reserved)
{
	// preset
	int result = 1;
	switch(reason)
	{
		case DLL_PROCESS_ATTACH:
			result = 1;
			break;
		case DLL_PROCESS_DETACH:
			result = 1;
			break;
		case DLL_THREAD_ATTACH:
			result = 1;
			break;
		case DLL_THREAD_DETACH:
			result = 1;
			break;
	}
	return 1;
}


//testing
EXPORT void Test()
{

}


#endif