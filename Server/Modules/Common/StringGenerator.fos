// FOS Common

LOG_MODULE_COMPILE

#ifndef STRING_GENERATOR
#define STRING_GENERATOR

shared interface IStringGenerator
{
	void SetCollection(string collection);
	void SetSeed(uint seed);
	string Generate(uint len);
}

shared class GenericStringGenerator : IStringGenerator
{	

	protected string collection = "default";
	protected uint   seed 	  = 0; // unused


	GenericStringGenerator()
	{

	}

	GenericStringGenerator(string collection)
	{
		this.SetCollection(collection);
	}

	void SetCollection(string collection) override
	{
		this.collection = collection;
	}

	void SetSeed(uint seed) override
	{
		this.seed = seed;
	}

	string Generate(uint len) override
	{
		string result = "";
		if(len > 0)
		{
			result.rawResize(len);
			uint cLen = this.collection.length();
			if(cLen > 0)
			{
				for(uint i = 0; i < len; i++)
				{
					result.rawSet(i, this.collection.rawGet(::Random(0, cLen -1)));
				}
			}
			else
			{
				for(uint i = 0; i < len; i++)
				{
					result.rawSet(i, ' ');
				}
			}
		}
		return result;
	}
}


// same as generic but also checks strings for being unique internally
// that will make sure that all strings it generates will be unique while its alive
shared class CollisionStringGenerator : GenericStringGenerator
{
	::dict<hash, string> generated = {};

	CollisionStringGenerator()
	{
		super();
	}

	CollisionStringGenerator(string collection)
	{
		super(collection);
	}


	string Generate(uint len) override
	{
		string result = GenericStringGenerator::Generate(len); // generate regular one
		hash strHash = Crypto::MurmurHash2(result);
		while(this.generated.exists(strHash)) // can become eternal loop, careful.
		{
			result  = GenericStringGenerator::Generate(len);
			strHash = Crypto::MurmurHash2(result);
		}

		this.generated.set(strHash, result);
		return result;
	}
}

#endif