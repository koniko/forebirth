// FOS Client

LOG_MODULE_COMPILE

#ifndef CUSTOMCALL_MODULE
#define CUSTOMCALL_MODULE

const string CUSTOMCALL_LOGIN = "Login";
const string CUSTOMCALL_REGISTER = "Register";
const string CUSTOMCALL_CONNECT  = "CustomConnect";
const string CUSTOMCALL_DUMP_ATLASES = "DumpAtlases";
const string CUSTOMCALL_SWITCH_SHOWTRACK = "SwitchShowTrack";
const string CUSTOMCALL_SWITCH_SHOWHEX   = "SwitchShowHex";
const string CUSTOMCALL_SWITCH_FULLSCREEN = "SwitchFullscreen";
const string CUSTOMCALL_MINIMIZE_WINDOW   = "MinimizeWindow";
const string CUSTOMCALL_SWITCH_SHOOT	  = "SwitchShootBorders";
const string CUSTOMCALL_SWITCH_LOOK  	  = "SwitchLookBorders";
const string CUSTOMCALL_GET_SHOOT_BORDERS = "GetShootBorders";
const string CUSTOMCALL_SET_SHOOT_BORDERS = "SetShootBorders";
const string CUSTOMCALL_SET_MOUSE_POS     = "SetMousePos";
const string CUSTOMCALL_SET_CURSOR_POS    = "SetCursorPos";
const string CUSTOMCALL_NET_DISCONNECT    = "NetDisconnect";
const string CUSTOMCALL_TRY_EXIT		  = "TryExit";
const string CUSTOMCALL_GET_VERSION		  = "Version";
const string CUSTOMCALL_GET_BYTES_RECIEVED    = "BytesReceive";
const string CUSTOMCALL_GET_BYTES_SEND		  = "BytesSend";
const string CUSTOMCALL_GET_LANGUAGE 	  = "GetLanguage";
const string CUSTOMCALL_SET_LANGUAGE	  = "SetLanguage";
const string CUSTOMCALL_SET_RESOLUTION    = "SetResolution";
const string CUSTOMCALL_REFRESH_ALWAYS_ONTOP	= "AlwaysOnTop";
const string CUSTOMCALL_COMMAND					= "Command";
const string CUSTOMCALL_CONSOLE_MESSAGE			= "ConsoleMessage";
const string CUSTOMCALL_SAVE_LOG				= "SaveLog";
const string CUSTOMCALL_DIALOG_ANSWER			= "DialogAnswer";
const string CUSTOMCALL_DRAW_MINIMAP			= "DrawMiniMap";
const string CUSTOMCALL_REFRESH_ME				= "RefreshMe";
const string CUSTOMCALL_SET_CRITTERS_CONTOUR	= "SetCrittersContour";
const string CUSTOMCALL_DRAW_WAIT				= "DrawWait";
const string CUSTOMCALL_CHANGE_DIR				= "ChangeDir";
const string CUSTOMCALL_MOVE_ITEM				= "MoveItem";
const string CUSTOMCALL_SKIP_ROOF				= "SkipRoof";
const string CUSTOMCALL_REBUILD_LOOK_BORDERS    = "RebuildLookBorders";
const string CUSTOMCALL_TRANSIT_CRITTER			= "TransitCritter";
const string CUSTOMCALL_SEND_MOVE				= "SendMove";
const string CUSTOMCALL_CHOSEN_ALPHA			= "ChosenAlpha";
const string CUSTOMCALL_SET_SCREEN_KEYBOARD		= "SetScreenKeyboard";

const string CUSTOMCALL_DELIMETER = " ";
const uint CUSTOMCALL_DELIMETER_LENGTH = CUSTOMCALL_DELIMETER.length();

void Login(string name, string password)
{
	if(!name.isEmpty() && !password.isEmpty())
	{
		::CustomCall(CUSTOMCALL_LOGIN + CUSTOMCALL_DELIMETER + name + CUSTOMCALL_DELIMETER + password, CUSTOMCALL_DELIMETER);
	}
}

void Register(string name, string password)
{
	if(!name.isEmpty() && !password.isEmpty())
	{
		::CustomCall(CUSTOMCALL_REGISTER + CUSTOMCALL_DELIMETER + name + CUSTOMCALL_DELIMETER + password, CUSTOMCALL_DELIMETER);
	}
}

void Connect()
{
	::CustomCall(CUSTOMCALL_CONNECT, CUSTOMCALL_DELIMETER);
}

void DumpAtlases()
{
	::CustomCall(CUSTOMCALL_DUMP_ATLASES, CUSTOMCALL_DELIMETER);
}

void SwitchShowTrack()
{
	::CustomCall(CUSTOMCALL_SWITCH_SHOWTRACK, CUSTOMCALL_DELIMETER);
}

void SwitchShowHex()
{
	::CustomCall(CUSTOMCALL_SWITCH_SHOWHEX, CUSTOMCALL_DELIMETER);
}

void SwitchFullscreen()
{
	::CustomCall(CUSTOMCALL_SWITCH_FULLSCREEN, CUSTOMCALL_DELIMETER);
}

void SwitchLookBorders()
{
	::CustomCall(CUSTOMCALL_SWITCH_LOOK, CUSTOMCALL_DELIMETER);
}

void SwitchShootBorders()
{
	::CustomCall(CUSTOMCALL_SWITCH_SHOOT, CUSTOMCALL_DELIMETER);
}

void MinimizeWindow()
{
	::CustomCall(CUSTOMCALL_MINIMIZE_WINDOW, CUSTOMCALL_DELIMETER);
}

bool GetShootBorders()
{
	string ret = ::CustomCall(CUSTOMCALL_GET_SHOOT_BORDERS, CUSTOMCALL_DELIMETER);
	bool converted = false;
	Strings::StrToBool(ret, converted);
	return converted;
}

void SetShootBorders(bool state)
{
	::CustomCall(CUSTOMCALL_SET_SHOOT_BORDERS + CUSTOMCALL_DELIMETER + state, CUSTOMCALL_DELIMETER);
}


// track shows if engine should calculate mouse position delta and raise mouse move event with it
void SetMousePos(int x, int y, bool track)
{
	::CustomCall(CUSTOMCALL_SET_MOUSE_POS + CUSTOMCALL_DELIMETER + x + CUSTOMCALL_DELIMETER + y + CUSTOMCALL_DELIMETER + track, CUSTOMCALL_DELIMETER);
}

void SetCursorPos()
{
	::CustomCall(CUSTOMCALL_SET_CURSOR_POS, CUSTOMCALL_DELIMETER);
}

void NetDisconnect()
{
	::CustomCall(CUSTOMCALL_NET_DISCONNECT, CUSTOMCALL_DELIMETER);
}

void TryExit()
{
	::CustomCall(CUSTOMCALL_TRY_EXIT, CUSTOMCALL_DELIMETER);
}

string GetVersion()
{
	return ::CustomCall(CUSTOMCALL_GET_VERSION, CUSTOMCALL_DELIMETER);
}

string GetBytesSend()
{
	return ::CustomCall(CUSTOMCALL_GET_BYTES_SEND, CUSTOMCALL_DELIMETER);
}

string GetBytesRecieved()
{
	return ::CustomCall(CUSTOMCALL_GET_BYTES_RECIEVED, CUSTOMCALL_DELIMETER);
}

string GetLanguage()
{
	return ::CustomCall(CUSTOMCALL_GET_LANGUAGE, CUSTOMCALL_DELIMETER);
}

// 4 chars length always
void SetLanguage(string lang)
{
	::CustomCall(CUSTOMCALL_SET_LANGUAGE + CUSTOMCALL_DELIMETER + lang, CUSTOMCALL_DELIMETER);
}


void SetResolution(int x, int y)
{
	::CustomCall(CUSTOMCALL_SET_RESOLUTION + CUSTOMCALL_DELIMETER + x + CUSTOMCALL_DELIMETER + y, CUSTOMCALL_DELIMETER);
}

void RefreshAlwaysOnTop()
{
	::CustomCall(CUSTOMCALL_REFRESH_ALWAYS_ONTOP, CUSTOMCALL_DELIMETER);
}

#ifdef DONT_DEFINE

static const CmdDef CmdList[] =
{
    { "exit", CMD_EXIT },
    { "myinfo", CMD_MYINFO },
    { "gameinfo", CMD_GAMEINFO },
    { "id", CMD_CRITID },
    { "move", CMD_MOVECRIT },
    { "disconnect", CMD_DISCONCRIT },
    { "toglobal", CMD_TOGLOBAL },
    { "prop", CMD_PROPERTY },
    { "getaccess", CMD_GETACCESS },
    { "additem", CMD_ADDITEM },
    { "additemself", CMD_ADDITEM_SELF },
    { "ais", CMD_ADDITEM_SELF },
    { "addnpc", CMD_ADDNPC },
    { "addloc", CMD_ADDLOCATION },
    { "reloadscripts", CMD_RELOADSCRIPTS },
    { "reloadclientscripts", CMD_RELOAD_CLIENT_SCRIPTS },
    { "rcs", CMD_RELOAD_CLIENT_SCRIPTS },
    { "runscript", CMD_RUNSCRIPT },
    { "run", CMD_RUNSCRIPT },
    { "reloadprotos", CMD_RELOAD_PROTOS },
    { "regenmap", CMD_REGENMAP },
    { "reloaddialogs", CMD_RELOADDIALOGS },
    { "loaddialog", CMD_LOADDIALOG },
    { "reloadtexts", CMD_RELOADTEXTS },
    { "settime", CMD_SETTIME },
    { "ban", CMD_BAN },
    { "deleteself", CMD_DELETE_ACCOUNT },
    { "changepassword", CMD_CHANGE_PASSWORD },
    { "changepass", CMD_CHANGE_PASSWORD },
    { "log", CMD_LOG },
    { "exec", CMD_DEV_EXEC },
    { "func", CMD_DEV_FUNC },
    { "gvar", CMD_DEV_GVAR },
};

#endif // DONT_DEFINE

string SendCommand(string command)
{
	return ::CustomCall(CUSTOMCALL_COMMAND + CUSTOMCALL_DELIMETER + command, CUSTOMCALL_DELIMETER);
}

void ConsoleMessage(string msg)
{
	::CustomCall(CUSTOMCALL_CONSOLE_MESSAGE + CUSTOMCALL_DELIMETER + msg, CUSTOMCALL_DELIMETER);
}

void DialogAnswer(bool isNpc, uint talker_id, uint answer_index)
{
	::CustomCall(CUSTOMCALL_DIALOG_ANSWER + CUSTOMCALL_DELIMETER + isNpc + CUSTOMCALL_DELIMETER + talker_id + CUSTOMCALL_DELIMETER + answer_index, CUSTOMCALL_DELIMETER);
}


void DrawMiniMap(int zoom, int x1, int y1, int x2, int y2)
{
	::CustomCall(CUSTOMCALL_DRAW_MINIMAP + CUSTOMCALL_DELIMETER + zoom + CUSTOMCALL_DELIMETER + x1 + CUSTOMCALL_DELIMETER + y1 + CUSTOMCALL_DELIMETER + x2 + CUSTOMCALL_DELIMETER + y2, CUSTOMCALL_DELIMETER);
}

void RefreshMe()
{
	::CustomCall(CUSTOMCALL_REFRESH_ME, CUSTOMCALL_DELIMETER);
}

void SetCrittersContour(int contour_type)
{
	::CustomCall(CUSTOMCALL_SET_CRITTERS_CONTOUR + CUSTOMCALL_DELIMETER + contour_type, CUSTOMCALL_DELIMETER);
}

void DrawWait()
{
	::CustomCall(CUSTOMCALL_DRAW_WAIT, CUSTOMCALL_DELIMETER);
}

void ChangeDir(uint8 dir)
{
	::CustomCall(CUSTOMCALL_CHANGE_DIR + CUSTOMCALL_DELIMETER + dir, CUSTOMCALL_DELIMETER);
}

void MoveItem(uint count, uint id, uint swap_id, int toslut)
{
	::CustomCall(CUSTOMCALL_MOVE_ITEM + CUSTOMCALL_DELIMETER + count + CUSTOMCALL_DELIMETER + id  + CUSTOMCALL_DELIMETER + swap_id + CUSTOMCALL_DELIMETER + toslut, CUSTOMCALL_DELIMETER);
}

void RebuildLookBorders()
{
	::CustomCall(CUSTOMCALL_REBUILD_LOOK_BORDERS, CUSTOMCALL_DELIMETER);
}

void SkipRoof(uint16 hx, uint16 hy)
{
	::CustomCall(CUSTOMCALL_SKIP_ROOF + CUSTOMCALL_DELIMETER + hx + CUSTOMCALL_DELIMETER + hy, CUSTOMCALL_DELIMETER);
}

void TransitCritter(int hx, int hy, bool animate, bool force)
{
	::CustomCall(CUSTOMCALL_TRANSIT_CRITTER + CUSTOMCALL_DELIMETER + hx + CUSTOMCALL_DELIMETER + hy + CUSTOMCALL_DELIMETER + animate + CUSTOMCALL_DELIMETER + force, CUSTOMCALL_DELIMETER);
}

void SendMove(array<uint8>@ path)
{
	if(valid(path))
	{
		uint dirsc = path.length();
		if(dirsc > 0 )
		{
			Critter@ chosen = ::GetChosen();
			if(valid(chosen))
			{
				string dump;
			//	uint dumpSize = dirsc + ( (dirsc-1) * CUSTOMCALL_DELIMETER_LENGTH); 
			//	uint end = dumpSize - 1;
			//	dump.rawResize(dumpSize);
			//	uint offs = 0;
            
				for(uint i = 0; i < dirsc-1; i++)
				{
					dump += path[i] + CUSTOMCALL_DELIMETER;
				}

                dump += path[dirsc-1];
                ::Log( dump );
				//::CustomCall(CUSTOMCALL_SEND_MOVE + " " + 0);
				//::CustomCall(CUSTOMCALL_SEND_MOVE + CUSTOMCALL_DELIMETER + dump, CUSTOMCALL_DELIMETER);
			}
		}
	}
}

void SetChosenAlpha(uint8 alpha)
{
	::CustomCall(CUSTOMCALL_CHOSEN_ALPHA + CUSTOMCALL_DELIMETER + alpha, CUSTOMCALL_DELIMETER);

}

// i dont know what is this so TODO
 // else if( cmd == "SetScreenKeyboard" && args.size() == 2 )
 //    {
 //        if( SDL_HasScreenKeyboardSupport() )
 //        {
 //            bool cur = ( SDL_IsTextInputActive() != SDL_FALSE );
 //            bool next = _str( args[ 1 ] ).toBool();
 //            if( cur != next )
 //            {
 //                if( next )
 //                    SDL_StartTextInput();
 //                else
 //                    SDL_StopTextInput();
 //            }
 //        }
 //    }
void SetScreenKeyboard()
{

}

#endif